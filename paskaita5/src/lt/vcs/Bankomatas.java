package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Bankomatas {
    
    private int cash = 500;
    private final String pass = "test";
    
    public boolean logIn(String pass){
        //prisijungti su pin kodu test
        if (pass.equals(this.pass)) {
            out("sekmingai prisijungiau");
            return true;
        } else {
            /*out("neteisingai pin kodas");
            return false;*/
            return logIn(inStr("neteisingai ivestas slaptazodis, Iveskite kita"));
        }
        
    }
    
    public int isimti(int sum){
        //minusuos paduoda suma
        cash = cash - sum;
        return cash;
    }

    public int getCash() {
        return cash;
    }
 
}
