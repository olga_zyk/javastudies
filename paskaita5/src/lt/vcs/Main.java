package lt.vcs;

import static lt.vcs.VcsUtils.*;
import lt.vcs.Bankomatas.*;

/**
 *
 * @author Olga
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bankomatas bank = new Bankomatas();
        int sum = 0;
        while (bank.getCash() != 0) {
            Person per = new Person(inStr("Iveskite savo varda"));
            String pass = inStr("Iveskite pin koda");
            bank.logIn(pass);
            out("Laba diena, " + per.getName() + ", Bankomate yra " + bank.getCash() + " pinigu");
            out("Kiek pinigu norite isimti?");
            sum = (inInt());
            sum = bank.isimti(sum);
            out("Bankomate liko " + bank.getCash());
        }
        out("Bankomate nebeliko pinigu, bankomatas issijungia");
    }

}
