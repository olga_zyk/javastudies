package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Deimantas.*;

/**
 *
 * @author Olga
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        String words = inLine("Iveskite zodzius atskirtus kableliu");
//        String[] wordsMas = words.replaceAll(" ", "").split(",");
//        int[] mas = new int[3];
//        int[][] intMas = {
//            {1, 2, 3},
//            {4, 5, 6},
//            {7, 8, 9}
//        };
//        out(" " + intMas[1][1]);     //out 5
//        List<String> pvz = new ArrayList();
//        pvz.add("kazkas");
//        List<String>/*class*/ listas = toSortedList(wordsMas);
//        //List<Integer> intLst = toSortedList(3, 2, 1);
//        out("nesurusiuoti zodziai");
//        for (String word : listas) {
//            out(word);
//        }
//        Collections.sort(listas);
//        Set<String> setas = new TreeSet(listas);
//        out("surusiuoti zodziai");
//        for (String word : setas) {
//            out(word);
//        }
//        Map<String, List<String>> mapas = new HashMap();
//        List<String> neSu = Arrays.asList("banabas", "ananasas", "agurkas");
//        mapas.put("nesurusiuotas", neSu);
//        mapas.put("surusiuotas", listas);
////        out(mapas.get(2));
//        for (String word : mapas.get("nesurusiuotas")) {
//            out(word);
//        }
//        Deimantas<List<Integer>> bla = null;

        String eilute = inLine("Iveskite teksta");
        String isvalytaEilute = eilute.replaceAll(" ", "").replaceAll("'.'", "");
        char[] raides = isvalytaEilute.toCharArray();
        List<String> strRaides = charArrToStrList(raides);
        Map<String, Integer> mapas = new HashMap();
        for (String raide : strRaides) {
            Integer value = mapas.get(raide);
            if (value == null) {
                mapas.put(raide, 1);
            } else {
                mapas.put(raide, value+1);
            }
        }
        List<Integer> values = new ArrayList(mapas.values());
        Collections.sort(values);
        Collections.reverse(values);
        out("Panaudotos raides mazejimo tvarka, ignoruojant ar mazasias raides: ");
        for (Integer sk : values) {
            List<String> panaudotos = new ArrayList();
            for (String key : mapas.keySet()) {
                Integer val = mapas.get(key);
                if (!panaudotos.contains(key) && sk.equals(val)) {
                    panaudotos.add(key);
                    out(key + " - " + sk);
                    break;
                } 
            }
            
        }
    }

    private static List<String> charArrToStrList(char[] chars) {
        List<String> result = new ArrayList();
        for (char charas : chars) {
            result.add(("" + charas).toLowerCase());
        }
        return result;
    }

    private static <E extends String> List<E> toSortedList(E... strMas) {
        List listas = Arrays.asList(strMas);
        Collections.sort(listas);
        return listas;

    }
}
