package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs";
        String dbUser = "root";
        String dbPass = "";
        Connection conn = DriverManager.getConnection(url+dbName, dbUser, dbPass);
        if (conn != null) {
            out("Valio prisijungem prie " + url + dbName);
        }
        conn.setAutoCommit(false);
        String name = inStr("Iveskite varda");
        String surname = inStr("Iveskite pavarde");
        int age = inInt("Iveskite amziu");
        Person p = new Person(getLastPersonId(conn)+1, name, surname, age);
        if (p.save(conn)) {
            out("Person(id=" + p.getId() + ") objectas issaugotas");
        } else {
            out("Person objekto issaugot nepavyko");
        }
        conn.commit();
        conn.close();
        
    } 
    
    private static int getLastPersonId(Connection conn) throws Exception{
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select id from person order by id desc");
        rs.next();
        return rs.getInt("id");
    }
    
    private static boolean savePersonToDB(Connection conn, Person p) throws Exception {
        Statement st = conn.createStatement();
        int res = st.executeUpdate("");
        
        return true;
    }

}
