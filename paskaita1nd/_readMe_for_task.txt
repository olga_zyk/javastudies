TASK:


1. Task1:

Parasyti programa kuri paprasytu vartotojo ivesti zodi ir ji is karto atvaizduotu, tada vel paprasytu ivesti kita zodi, ji atvaizduotu, ir procesa kartotu tol, kol nebus ivestas zodis „pabaiga“.

2. Task2:

Parasyti programa, kuri paprasytu vesti skaicius tol, kol bus ivestas skaicius 0. Pabaigoje turi buti atvaizduojama ivestu skaiciu suma.
P.S. Stringus lyginam naudojant equals metoda, pvz.: kazkoxStringas.equals("kazkas");