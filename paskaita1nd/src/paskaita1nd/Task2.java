/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paskaita1nd;

import java.util.Scanner;

/**
 *
 * @author Olga
 */
public class Task2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int number = 0;
        int sum = 0;

        do {
            System.out.println("Please, enter the integer number");
            number = (new Scanner(System.in)).nextInt();

            sum = number + sum;

        } while (number != 0);
        System.out.println(sum);

    }

}
