package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Arklys extends Gyvunas {
    
    @Override
    public void gyvent(){
        out("Arklys gyvena");
        gyventKaipGyvunai();
    }

    @Override
    public String getWorld() {
        return "land";
    }
    
}
