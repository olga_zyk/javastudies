package lt.vcs;

/**
 *
 * @author Olga
 */
public enum Banknotas {
    
    PENKI(5, "penki"), //declarated constant
    DESIMT(10, "desimt"),
    DVIM(20, "dvidesimt"),
    PEM(50, "penkiasdesimt"),
    SIMTAS(100, "simtas");
    
    private int sk;
    private String label;
    
    private Banknotas(int sk) {
        this.sk = sk;
        this.label = label;
    }
    
    private Banknotas(int sk, String label) {
        this.sk = sk;
    }

    public int getSk() {
        return sk;
    }

    public void setSk(int sk) {
        this.sk = sk;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
}
