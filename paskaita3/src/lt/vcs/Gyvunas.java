package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author agleH
 */
public abstract class Gyvunas implements Creature {
    
    public abstract void gyvent();
    
    public void gyventKaipGyvunai(){
        out("Gyvenu kaip gyvunas");
    }
    
    @Override
    public String toString(){
        return this.getClass().getName() + " " + getWorld();
    }
}
