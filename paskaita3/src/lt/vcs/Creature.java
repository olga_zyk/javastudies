package lt.vcs;

/**
 *
 * @author Olga
 */
public interface Creature {
    
    /**
     * metodas kuris nusako kur karaliaauja sis padaras, ar vandeny, ar dauguje, ar zemes
     * @return 
     */
    public abstract String getWorld();
}
