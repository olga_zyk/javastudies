package lt.vcs;

/**
 *
 * @author Olga
 */
public enum Gender {

    MALE(1, "male"),
    FEMALE(2, "female"),
    OTHER(3, "trans-gender or other circumstance");

    private final int id;
    private final String description;

    private Gender(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public static Gender getById(int id) {
        switch(id){
            case 1:
                return MALE;
            case 2:
                return FEMALE;
            case 3:
                return OTHER;
            default:
                return null;
        }
    }
    
    public int getId(int id) {
        return id;
    }

    public String getDescription() {
        return description;
    }

}
