package lt.vcs;

import java.util.Date;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Banknotas.*;
import lt.vcs.Gender.*;
import lt.vcs.Person.*;

/**
 *
 * @author Olga
 */
public class Main {

    private static final String PIN = "test";

    private static String programName;
    private static Date creationDate;
    private static String creator;

    public Main(String name, String programmer) {
        this(name);
        this.creator = programmer;
    }

    public Main(String name) {
        super();
        if (name == null || name.isEmpty()) {
            out("programa turi tureti pavadinima");
            System.exit(-1);                            //system error
        }
        //String programName = "blezvyzgos";
        this.programName = name;
        this.creationDate = new Date();
    }

    /**
     * @param args the command line arguments
     */
    /*
    Bankomate yra 350 pinigų, naudotojas paleidęs programą paprašomas įvesti pin kodą, 
    kuris yra "test", jei kodas teisingas naudotojas gauna pranešimą, kad sėkmingai prisijungė ir kad bankomate/saskaitoj 
    yra jau minėta suma pinigų ir paprašo įvesti kiekį, kurį jis nori išsigryninti, kiekis negali buti didesnis uz 
    saskaitos likuti, įvedus išvedamas tekstas jog pinigai išgryninti ir saskaitos likutis, programa baigia darbą.
     */
    public static void main(String[] args) {
        
        paskaita4Kodas();
 //METHOD:
//        paskaita4Asmuo(); //write created Method which will return out(asmuo);
//        paskaita3Kodas();
    }

    private static void paskaita4Kodas(){
        Object o = new Object();
        out("Object: " + o);
        o = new Arklys();
        out("Arklys: " + o);
        o = new Vienaragis();
        out("Vienaragis: " + o);
        
        Creature c = new Arklys();
        Arklys ark = null;
        if (c instanceof Arklys) {
            ark = (Arklys)c; //convertation "c"(Creature) to Arklys?
        }
        gyvenk(ark);
        out("Arklys: " + c);
        c = new Vienaragis();
        out("Vienaragis: " + c);
        
        Gyvunas g = new Arklys();
        gyvenk(g);
        out("Arklys: " + c);
        c = new Vienaragis();
        gyvenk(g);
        out("Vienaragis: " + c);
        gyvenk(new Arklys());
        
        if (o instanceof Object) {
            out("o yra Objectas");
        }
        if (o instanceof Creature) {
            out("o yra Creature");
        }
        if (o instanceof Gyvunas) {
            out("o yra Gyvunas");
        }
        if (o instanceof Vienaragis) {
            out("o yra Vienaragis");
        }
    }
    
    private static void gyvenk(Gyvunas g){
        g.gyvent();
    }
    
    private static void paskaita3Kodas(){
        //String programName = "blezvyzgos"; //was moved to constructor
        out(programName);
        int likutis = 350;
        String pin = inStr("Iveskite pin koda");
        if (PIN.equals(pin)) {
            //cia jei teisingai bus vykdomas kodas
            out("Jusu saskaitoje yra: " + likutis + " " + "pinigu");
            out("Galite pasirinkti tik viena banknota, kiek noresite issigryniti");
            out("Banknotu nominalai: ");
            for (Banknotas bnkn : Banknotas.values()) {
                out(bnkn.getLabel());
            }
            int isemam = inInt("Koki banknota norite issigryniti?");
            if (isemam <= 0 || isemam > likutis) {
                out("Neteisinga suma. Programa baigs");
            } else {
                Banknotas bnkn = suraskBanknota(isemam);
                out("Jums isgryninta: " + isemam + " " + "pinigu");
                out("Jusu saskaitos likutis: " + (likutis - bnkn.getSk()));
            }
        } else {
            out("Kodas neteisingas. Programa baigs");
        }
        //        DVIM.getSk()
    }
    
    private static void paskaita4Asmuo() {   //create a Method
        String name = inStr("Iveskite varda");
        String surname = inStr("Iveskite ravarde");
        int age = inInt("Iveskite amziu");
        int gen = inInt("Pasirinkite lyti: 1-vyras, 2-moteris, 3-kita");
        Person asmuo = new Person(name, surname, age, Gender.getById(gen));
        out(asmuo);
    }

    private static Banknotas suraskBanknota(int sk) {
        Banknotas result = null;
        for (Banknotas bnkn : Banknotas.values()) {
            if (bnkn.getSk() == sk) {
                result = bnkn;
                break;
            }

        }
        return result;

    }
}
