package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Vienaragis extends Gyvunas {

    @Override
    public void gyvent() {
        out("Vienaragis gyvena");
        gyventKaipGyvunai();
    }

    @Override
    public String getWorld() {
        return "land";
    }
    
}
