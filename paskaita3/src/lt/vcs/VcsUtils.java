package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Olga
 */
public class VcsUtils {
    
    private final MainGender Gender = new MainGender();
    
    private final Main pvz = new Main("test");
    
    public static void out(Object txt){
        System.out.println(txt);
        //System.out.println(timeNow() + " " + txt);
    }
    
    public static String inStr(String txt){
        out(txt);
        return inStr();
    }
    
    public static String inStr(){
        return newScan().next();
    }
    
    public static int inInt(String txt){
        out(txt);
        return inInt();
    }
    
    public static String inLine(String txt){ //for second task
        out(txt);
        return inLine();
    }
    
    public static String inLine(){      //for second task
        return newScan().nextLine();
    }
    
    public static int inInt(){
        return newScan().nextInt();
    }
    
    private static Scanner newScan(){
        return new Scanner(System.in);
    }
    
    private static String timeNow(){
        SimpleDateFormat sdf = new SimpleDateFormat("'['HH:mm:ss.SS']'");
        return sdf.format(new Date());
    }
    
    public static int random(int from, int to){
        return ThreadLocalRandom.current().nextInt(from, to +1);
    }

}
