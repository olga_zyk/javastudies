/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Olga
 */
public class Person {

    private String fName;
    private String lName;
    private Integer age;
    private Gender gender;

    public Person(String fName, String lName, Integer age, Gender gender) {
        this.fName = fName;
        this.lName = lName;
        this.age = age;
        this.gender = gender;
    }
    
    @Override
    public String toString(){
        return super.toString() + "Person(vardas: " + fName + "; pavarde: " + lName + ")";
}
    public String getfName(){
        return fName;
}

}

