package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {   //throws Exception = "throws" made cause file constructor can throws exception in the case that file doesn't exist   
        //example 
        /*File failas = new File("C:/pvz.txt");
        out("Failas egzistoja? " + failas.exists());*/

        String kelias = inStr("Iveskite pilna failo kelio");
        File failas = new File(kelias);
        out("Failas egzistoja? " + failas.exists());
        String failoTurinys = null;
        try {
            BufferedReader br2 = createBR(failas);
            failoTurinys = readTextFile(br2);
            br2.close();
        } catch (Exception e) {
            throw new RuntimeException("betkokias tekstas", e);
        }

        try {
            BufferedWriter bw = createBW(failas);
            bw.append(failoTurinys);
            bw.newLine();
            bw.append("pirmas irasimas");
            bw.flush();
            bw.close();
        } catch (Exception e) {
            throw new RuntimeException("betkokias tekstas", e);
        }

        try {

            BufferedReader br = createBR(failas);
            out("txt failo turinys:" + NL + readTextFile(br));
            br.close();                                                     //to close all hierarchy from fis to br. All streams should be closed
        } catch (Exception e) {
            throw new RuntimeException("betkokias tekstas-1", e);
        }

    }



}
