
import java.util.InputMismatchException;
import java.util.Scanner;

/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Olga
 */
public class IveskiteSkaicius {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
                
        int ivestas = 0;
        while (true) {
            System.out.println("Iveskite sveikaji teigiama arba neigiama skaiciu");
            
            try {
                ivestas = (new Scanner(System.in)).nextInt();
            } catch (InputMismatchException e) {
                System.out.println("EXIT");
                return;
            }

            if (ivestas >= 0) {
                System.out.println("teigiamas");
            } else {
                System.out.println("neigiamas");
            }
        }

    }

}
