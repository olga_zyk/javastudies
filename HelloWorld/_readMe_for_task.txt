TASKS:


1. "HelloWordApp":

	Parasyti Programa (toliau trumpinsiu - PP), kuri suskaiciuotu ir parodytu(komandinej eilutej isvestu) tesinga atsakyma: 2+2*2
	P.S. jei reikia, string'ai (tekstinis duomenu tipas java kalboj - String) jungiami paprasciausiu pliusu, pvz.: "Hello World" + " reiskia Labas Pasauli", arba kitas duomenu tipas gali buti prijungtas "Hello"+intTipoKintamojoPavadinimas


2. "IveskiteSkaicius":

	PP, kuri leistu vartotojui ivesti norima skaiciu ir po to parasytu ar sis skaicius yra teigiamas, ar neigiamas. Naudoti IF salyga, sintakse tokia pat kaip per C# ( if(salyga){} else {} )  
	P.S. sveiku skaiciu ivedimui is komandines eilutes naudokite:
System.out.println("Iveskite sveikaji teigiama arba neigiama skaiciu");
int ivestas = (new Scanner(System.in)).nextInt();
	P.P.S. failo virsuje reikia prideti importavimo nuoroda:
import java.util.Scanner;


3. "ForLoop": 

	Pasikartokite FOR cikla - PP, kuri i ekrana isvestu daugybos lentele nuo 1 iki 9 
	P.S. for ciklo sintakse tokia pat kaip C# ( for(int i=0; salyga; i++){} )


4*. "WhileLoop":

	* - reiskia optional challenge pasunkintas:
	PP, kuri priimtu teigiama skaiciu is komandines eilutes ir nuo 1 iki ivesto skaiciaus spausdintu daugybos lentele
	P.S. cia labiau tinka WHILE ciklas