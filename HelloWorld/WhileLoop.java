
import java.util.Scanner;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */

/**
 *
 * @author Olga
 */
public class WhileLoop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Iveskite sveikaji teigiama skaiciu");
        int ivestas = (new Scanner(System.in)).nextInt();
        int i = 1;
        //int columnIndex = 0;

        while (i <= ivestas) {

            for (int j = 1; j < 10; j++) {
                System.out.println(i + " x " + j + " = " + i * j);
            }
            i++;
            System.out.println();
        }
    }

}
