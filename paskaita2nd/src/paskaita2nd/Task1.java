package paskaita2nd;

import java.util.Scanner;

/**
 *
 * @author Olga
 */
public class Task1 {

    /*
    PP, kuri paprašytų įvesti 5 skaičius. 
    Baigus skaičių įvedimą, turi būti atvaizduojama įvestų skaičių suma ir visi įvesti skaičiai.
     */

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int sum = 0;
        System.out.println("Please enter 5 integer numbers");

        for (int i = 0; i <= 4; i++) {
            int number = (new Scanner(System.in)).nextInt();
            sum = sum + number;
        }
        System.out.println("sum = " + sum);
    }

}
