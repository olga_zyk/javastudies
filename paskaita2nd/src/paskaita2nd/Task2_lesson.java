
package paskaita2nd;

import static lt.vcs.VcsUtils.inLine;
import static lt.vcs.VcsUtils.out;

/**
 *
 * @author Olga
 */
public class Task2_lesson {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String line = inLine("Iveskite 5 zodius per kablele");
        String[] mas = new String[5];
        String newLine = line.replaceAll(" ", "");
        mas = newLine.split(",");
        for (String a : mas) {
            out(a);
        }
    }

}
