package paskaita2nd;

import java.io.PrintStream;
import static lt.vcs.VcsUtils.inLine;
import static lt.vcs.VcsUtils.out;

/**
 *
 * @author Olga
 */
public class Task3_lesson {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String line = inLine("Iveskite betkokio zodius per kableli");
        char[] mas = line.replaceAll(" ", "").replaceAll(" ", "").toCharArray();
        out("Viso zodius: " + mas.length);
        int aRaidSk = 0;
        for (char charas : mas) {
            if (charas == 'a') {
                aRaidSk++;
            }
        }
        out("viso a raidziu: " + aRaidSk);
    }
}
