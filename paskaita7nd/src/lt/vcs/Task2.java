package lt.vcs;

import java.io.BufferedReader;
import java.io.File;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Task2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String inPath = inStr("Iveskite pilna failo kelio");    //D:/_dev/JavaStudie/paskaita7nd/file.txt
        File file = new File(inPath);
        out("Failas egzistoja? " + file.exists());
        String failoTurinys = null;
        try {
            BufferedReader br2 = createBR(file);
            failoTurinys = readTextFile(br2);
            br2.close();
        } catch (Exception e) {
            throw new RuntimeException("betkokias tekstas", e);
        }
        
        
    }

}
