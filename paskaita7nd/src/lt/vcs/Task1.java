package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static lt.vcs.VcsUtils.inLine;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Olga
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String words = inLine("Iveskite zodzius atskirtus kableliu");
        String[] wordsMas = words.split(",");

        //"banana, arbuza mavruza, laba, daba, doo" - create an array
        //"banana,arbuza mavruza,laba,daba,doo"
        //"banana,arbuzamavruza,laba,daba,doo"
        //"banana" "arbuzamavruza" "laba" "daba" "doo"
        List<String> listas = Arrays.asList(wordsMas);
        List<String> listasTrimmed = new ArrayList<String>();
//        listas.forEach((t) -> {
//            listasTrimmed.add(t.trim());
//        });
        for (String word : listas) {
            if (!word.trim().isEmpty()) {
                listasTrimmed.add(word.trim());
            }
        }
        Collections.sort(listasTrimmed);
//        Collections.sort(listas);

        out("surusiuotas listas: " + listasTrimmed);
//        out("surusiuotas listas: " + listas);

    }

}
