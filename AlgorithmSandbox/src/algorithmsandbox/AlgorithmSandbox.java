package algorithmsandbox;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Olga
 */
public class AlgorithmSandbox {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int num = 0;
        int sum = 0;
        int count = 0;
        int countOdd = 0;
        int sumOdd = 0;
        
        do {
            System.out.println("Please enter integer numbers");
            num = new Scanner(System.in).nextInt();
            
            if (num % 2 == 0 && num != 0) {
                sum += num;
                count++;
                
            }else if (num % 2 != 0) {
                sumOdd +=num;
                countOdd++;
            }
        } while (num != 0);
        System.out.println("average: " + sum / count);
        System.out.println("even numbers: " + count);
        System.out.println("the sum of even numbers: " + sum);
        System.out.println("odd numbers: " + countOdd);
        System.out.println("the sum of odd numbers: " + sumOdd);
    }
}
