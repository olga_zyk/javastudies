package algorithmsandbox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Olga
 */
public class Task2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int even = 0;
        int odd = 0;
        
        System.out.println("Please enter integer numbers");
        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        String[] newLine = line.split(" ");
        int[] numbers = new int[newLine.length];
        ArrayList<Object> numList = new ArrayList<>();
        for (String newLine1 : newLine) {
            try {
                numList.add(Integer.parseInt(newLine1));
//                numbers[i] = Integer.parseInt(newLine[i]);
            }catch (NumberFormatException e) {
                continue;
            }
            //count odd and even numbers
            if ((int)numList.get(numList.size() - 1) %2 == 0){
                even++;
            } else {
                odd++;
            }
        }
        
        StringBuilder sbOdd = new StringBuilder();
        StringBuilder sbEven = new StringBuilder();
        for (Object numObj : numList) {
            if ((int)numObj %2 == 0) {
                sbEven.append(numObj).append(", ");
            } else {
                sbOdd.append(numObj).append(", ");
            }
        }
        System.out.println("entered even numbers: " + sbEven.substring(0, sbEven.length() - 2));
        System.out.println("entered odd numbers: " + sbOdd.substring(0, sbOdd.length() - 2));
        System.out.println("quantity of odd = " + odd);
        System.out.println("quantity of even = " + even);
        System.out.println("entered numbers: " + Arrays.toString(numbers));
        System.out.println("entered numbers: " + numList.toString());
        
    }
}
