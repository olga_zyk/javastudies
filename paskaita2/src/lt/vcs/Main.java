//one-line comment

/*
various
lines
comment
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static lt.vcs.VcsUtils.*;


/**
 * @author Olga
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //out("kazkas"); //in the case of static import we don't need the class name before like "VcsUtils.out"
        while (true) {
            String ivestas = inStr("Ivesk betkiki zodi");
            out(ivestas);
            if ((ivestas.equals("pabaiga")) || ivestas.equals("end") || ivestas.equals("exit")) {
                break;
            }

            int result = 0;
            while (true) {
                int i = inInt("Ivesk sveika skaiciu");
                if (i == 0) {
                    break;
                } else {
                    result += i;
                }
                out("rezultatas: " + result);

                switch (ivestas) {
                    case "pabaiga":
                    case "end":
                    case "exit":
                        break;
                    default:
                    //jei neatitiko neivienai salygai pries tai
                }

//        if (ivestas.equals("pabaiga")) {
//            //jei true vykdom cia
//        } else if (ivestas.equals("end")) {
//            //vykdom jei end, angliskai
//        } else if (ivestas.equals("exit")) {
//            //vykdom jei exit
//        } else {
//            //jei false vukdom cia
//        }
                //switch - should be used when we have two and more if condition
                switch (ivestas) {
                    case "pabaiga":
                        //kodas jei teksta pabaiga
                        break;
                    case "end":
                        //kodas jei teksta end
                        break;
                    case "exit":
                        //kodas jei teksta exit
                        break;
                    default:
                    //jei neatitiko neivienai salygai pries tai
                }
//        int i = inInt("ivesk sveika sk");
//        switch (i) {
//            case 1:
//            case 2:
//            case 3:
//                out("kazkox textas");
//                break;
//            case 4:
//            case 5:
//            case 6:
//                out("kazkox kitas textas");
//                break;
//            default:
//            //jei neatitiko neivienai salygai pries tai
//        }

                //for (; a > 0;) - also correct
//        for (int a=i; a>0; a--) {
//            if (i==3) {
//                continue;
//            }
//            if (i==1) {
//               break; 
//            }
//            out("" + i);
//        }
//        out("rezultate i lygus: " +i);
                //int[] mas = new int[i]; //array declaration
                int[] mas = i > 0 ? new int[i] : new int[5];  //another way to declarate an array - if ?true :false
                int[] mas2 = {1, 2, 3, 4, 5};
                String[] mas3 = {"one", "two", "three"};
                int[] mas4;
                Object o = mas3; // or Object o = "various string";
                List<String> mas5 = Arrays.asList(mas3); //array to list(список)
                
                String formatinsim = "mano batai buvo %d";
                out(String.format(formatinsim, 7));
                out(formatinsim);
                
//                "dsfsd".endsWith("fds");
//                "dsfds".contains("sfd");
//                "".isEmpty(); //true
//                "sdfsd".length();

//                String kint = "melagis, melagis";
//                if (kint != null) {
//                    kint.isEmpty();
//                }
//                kint = kint.replaceAll(" ", " ");
//                out(""+kint.matches("mel.*"));
//                String[] strMas = kint.split(",");
//                for (String a : strMas) {
//                    out(a);
//                }
//                
//                for (int a = mas.length-1; a > 0; i--) {
//                    mas[a] = a;
//                }
//                for (int a : mas) {
//                    out("" + a);
//                }
//                out("rezultate i lygus: " + i);
            
            //Date Format
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd '['HH:mm:ss']'");
            Date data = new Date(System.currentTimeMillis());
//            data.
            out(data);
            out(sdf.format(data));
                for (int ii = 0; ii < 5; ii++) {
                    out("random"+ii+": " + random(1,6));
                }
                
//                while (i > 0) {
//                    //do stuff
//                }
//
//                do {
//                    //do stuff at least once
//                } while (i > 0);



            }
        }
    }
}
